import cx_Freeze

# Replace with the actual name of your Python script
script_name = "grukos_uploader.py"

# Build a single executable file (replace with appropriate base)
base = None  # Create a console application by default
# base = "Win32GUI"  # Use for GUI applications with a graphical interface

executables = [
    cx_Freeze.Executable(
        script_name,
        base="Win32GUI",
        icon="icon.ico"
    )
]

cx_Freeze.setup(
    name="grukos_uploader",
    version="1.4.0",
    description="This application verifies and uploads shapefiles "
                "with groundwater-related data to the GRUKOS database.",
    executables=executables,
)