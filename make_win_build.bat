REM Run this script on a 64-bit Windows machine in a Anaconda Python 3 prompt

cd C:\gitlab\grukos_uploader

echo ### creating new conda virtualenv called exe ###
REM call conda create -n exe --yes python=3.9
REM call conda activate exe
REM pip install pyshp wxpython dis3 requests psycopg2 cx_Freeze
call conda activate grukos_uploader

echo ### Removing previous build directory ###
if exist "grukos_uploader\" rmdir /S /Q grukos_uploader
if exist "build\" rmdir /S /Q build
REM if exist "dist\" rmdir /S /Q dist
if exist "grukos_uploader.zip" del "*.zip" /s /f /q

echo ### Build stand-alone application ###
REM pyinstaller --onefile --windowed grukos_uploader.py --icon icon.ico
python setup.py build

echo ### Copy auxillary files to build directory ###
if not exist "grukos_uploader\" mkdir "grukos_uploader"
copy build\exe.win-amd64-3.9\grukos_uploader.exe grukos_uploader\
copy build\exe.win-amd64-3.9\python3.dll grukos_uploader\
copy build\exe.win-amd64-3.9\python39.dll grukos_uploader\
xcopy /S /E build\exe.win-amd64-3.9\lib grukos_uploader\lib\
REM copy dist\grukos_uploader.exe grukos_uploader\
copy LICENSE grukos_uploader\
copy icon.ico grukos_uploader\
copy README.md grukos_uploader\
copy config.ini grukos_uploader\
md grukos_uploader\templates
REM xcopy /S /E "F:\GKO\data\grukos\revideret_dataaflevering\Skabelon_nummer_GKO navn" grukos_uploader\templates
md grukos_uploader\sample_data
xcopy /S /E sample_data grukos_uploader\sample_data
md grukos_uploader\help
xcopy /S /E help grukos_uploader\help  

echo ### Zipping folder ###
SET sevenzip_path="C:\Program Files\7-Zip\7z.exe"
%sevenzip_path% a -tzip "C:\gitlab\grukos_uploader\grukos_uploader.zip" "C:\gitlab\grukos_uploader\grukos_uploader"

echo ### Cleaning up repo ###
REM rmdir /S /Q dist
rmdir /S /Q build
REM rmdir /S /Q __pycache__
REM del "*.spec" /s /f /q
rmdir /S /Q grukos_uploader

echo ### Removing conda virtualenv called exe ###
REM call conda activate base
REM call conda env remove -n exe --yes
pause
